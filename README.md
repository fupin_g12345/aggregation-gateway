## Structure

```
## Key Stack

1. Spring boot
2. OpenApi
3. JPA, MySQL, Mongo, Couchbase
4. Docker for local development and testing

## Setup Local Development

1. JDK11
2. maven

### Setup Intellij IDEA

1. plugin: lombok
2. plugin: google-java-format

### Local Build

```shell
mvn clean package -Dmaven.test.skip=true
```

### Local Start

Idea
1. select profile
2. Debug/Run UserApplication

### Docker

Now, it's only for local test, not for CI/CD


### Others

1. swagger: http://localhost:8080/swagger-ui.html

