package com.michaels.aggregationgateway;


import java.time.Instant;
import java.time.ZoneId;

public class test {
  public static void main(String[] args) {
    System.out.println(Instant.ofEpochMilli(System.currentTimeMillis()).atZone(ZoneId.of("UTC")).toLocalDateTime().toString());
  }
}
