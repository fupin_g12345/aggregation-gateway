package com.michaels.aggregationgateway.security.interceptor;

import com.michaels.aggregationgateway.security.auth.AuthContext;
import com.michaels.aggregationgateway.shared.common.BusinessCode;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

import com.michaels.aggregationgateway.shared.exception.InvalidCredentialsException;

@Aspect
@Component
public class RequireAuthPointCut {
  
  @Pointcut("@annotation(com.michaels.aggregationgateway.security.annotation.RequireAuth)")
  public void requireAuthMethods() {
    
  }
  
  @Before("requireAuthMethods()")
  public void requireAuthMethodsBefore(JoinPoint joinPoint) {
    checkRequireAuth(joinPoint);
  }
  

  @Pointcut("@within(com.michaels.aggregationgateway.security.annotation.RequireAuth)")
  public void requireAuthClasses() {
    
  }
  
  @Before("requireAuthClasses()")
  public void requireAuthClassesBefore(JoinPoint joinPoint) {
    checkRequireAuth(joinPoint);
  }
  

  private void checkRequireAuth(JoinPoint joinPoint) {
    AuthContext authContext=AuthContext.get();
    if(authContext==null) {
      throw new InvalidCredentialsException(BusinessCode.MCU_UNAUTHORIZED_ACCESS);
    }
  }
}
