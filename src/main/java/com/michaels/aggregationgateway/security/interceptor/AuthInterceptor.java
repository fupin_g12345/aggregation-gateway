package com.michaels.aggregationgateway.security.interceptor;

import com.michaels.aggregationgateway.security.auth.AuthContext;
import com.michaels.aggregationgateway.security.auth.AuthContextService;
import com.michaels.aggregationgateway.shared.util.ExceptionUtils;
import com.michaels.aggregationgateway.shared.util.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebFilter("/*")
@Component
public class AuthInterceptor extends HttpFilter {

  private static final String BEARER_PREFIX="Bearer ";

  @Override
  protected void doFilter(HttpServletRequest request, HttpServletResponse response, FilterChain chain)
      throws IOException, ServletException {

    // attach the AuthContext to current thread, if any
    try {
      String token=request.getHeader("Authorization");

      if(!StringUtils.isEmpty(token)) {
        if(StringUtils.startsWithIgnoreCase(token, BEARER_PREFIX)) {
          token=token.substring(BEARER_PREFIX.length());
        }

        WebApplicationContext context = WebApplicationContextUtils.getRequiredWebApplicationContext(request.getServletContext());
        AuthContextService authContextService = context.getBean(AuthContextService.class);
        AuthContext authContext = authContextService.fromJWT(token);
        authContext = authContext.hasExpired()?null:authContext;
        AuthContext.setAuthContext(authContext);
      } else {
        AuthContext.setAuthContext(null);
      }

    } catch (Exception e) {
      // set error in the header for testing, currently we cannot access to GCP environment
      String stackTraces = ExceptionUtils.getExceptionStackTrace(e);
      response.setHeader("error", stackTraces.replace("\n", ""));
      AuthContext.setAuthContext(null);
    }

    try {
      // process
      super.doFilter(request, response, chain);

    } finally {
      // clean
      AuthContext.setAuthContext(null);
    }
  }

  
}
