package com.michaels.aggregationgateway.security.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class JwtConfig {

  @Value("${jwt.signingKey: 8388-CF23-A037-EA9F}")
  private String jwtSigningKey;

  @Value("${jwt.signingKey: D2AF-9CA0-8D86-45C6}")
  private String twoFactorJwtSigningKey;

  public String getJwtSigningKey() {
    return jwtSigningKey;
  }

  public void setJwtSigningKey(String jwtSigningKey) {
    this.jwtSigningKey = jwtSigningKey;
  }

  public String getTwoFactorJwtSigningKey() {
    return twoFactorJwtSigningKey;
  }

  public void setTwoFactorJwtSigningKey(String twoFactorJwtSigningKey) {
    this.twoFactorJwtSigningKey = twoFactorJwtSigningKey;
  }
}
