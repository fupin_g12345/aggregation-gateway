package com.michaels.aggregationgateway.security.auth;

import com.michaels.aggregationgateway.security.configuration.JwtConfig;
import com.michaels.aggregationgateway.shared.util.StringUtils;
import io.jsonwebtoken.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;

@Service
public class AuthContextService {

  @Autowired
  private JwtConfig jwtConfig;

  public String toJWT(AuthContext authContext) {
    Map<String, Object> claims=new LinkedHashMap<String, Object>();
    claims.put(AuthContext._userId, StringUtils.nonull(authContext.getUserId()));
    claims.put(AuthContext._sellerStoreId, authContext.getSellerStoreId()==null?null:String.valueOf(authContext.getSellerStoreId()));
    claims.put(AuthContext._deviceUuid, authContext.getDeviceUuid());
    claims.put(AuthContext._deviceName, authContext.getDeviceName());
    claims.put(AuthContext._token, authContext.getToken());
    claims.put(AuthContext._createTime, String.valueOf(authContext.getCreateTime()));
    claims.put(AuthContext._expireTime, String.valueOf(authContext.getExpireTime()));

    String jwtToken=Jwts.builder()
        .setClaims(claims)
        .setSubject(Long.toString(authContext.getUserId()))
        .setIssuedAt(new Date(authContext.getCreateTime()))
        .setExpiration(new Date(authContext.getExpireTime()))
        .signWith(SignatureAlgorithm.HS256, jwtConfig.getJwtSigningKey())
        .compact();
    return jwtToken;
  }

  public AuthContext fromJWT(String jwtToken) {
    Claims claims = Jwts.parser()
        .setSigningKey(jwtConfig.getJwtSigningKey())
        .parseClaimsJws(jwtToken)
        .getBody();

    Long userId = Long.parseLong(claims.get(AuthContext._userId)+"");
    Long sellerStoreId = null;
    Object sellerStoreObj = claims.get(AuthContext._sellerStoreId);
    if(sellerStoreObj!=null && !StringUtils.isEmpty(sellerStoreObj.toString())) {
      sellerStoreId = Long.parseLong(sellerStoreObj.toString());
    }
    String deviceUuid = String.valueOf(claims.get(AuthContext._deviceUuid));
    String deviceName = String.valueOf(claims.get(AuthContext._deviceName));
    String token = String.valueOf(claims.get(AuthContext._token));
    Long createTime = Long.valueOf(claims.get(AuthContext._createTime)+"");
    Long expireTime = Long.valueOf(claims.get(AuthContext._expireTime)+"");

    AuthContext authContext=new AuthContext();
    authContext.setUserId(userId);
    authContext.setSellerStoreId(sellerStoreId);
    authContext.setDeviceUuid(deviceUuid);
    authContext.setDeviceName(deviceName);
    authContext.setToken(token);
    authContext.setCreateTime(createTime);
    authContext.setExpireTime(expireTime);

    return authContext;
  }

  public JwtConfig getJwtConfig() {
    return jwtConfig;
  }

  public void setJwtConfig(JwtConfig jwtConfig) {
    this.jwtConfig = jwtConfig;
  }
}
