package com.michaels.aggregationgateway.security.auth;

public class AuthContext {

  public static final String _userId = "_userId";
  public static final String _sellerStoreId = "_sellerStoreId";
  public static final String _token = "_token";
  public static final String _deviceUuid = "_deviceUuid";
  public static final String _deviceName = "_deviceName";
  public static final String _createTime = "_createTime";
  public static final String _expireTime = "_expireTime";

  public static final ThreadLocal<AuthContext> threadLocalInstanceHolder=new ThreadLocal<AuthContext>();
  
  public static void setAuthContext(AuthContext authContext) {
    threadLocalInstanceHolder.set(authContext);
  }
  
  public static AuthContext get() {
    return threadLocalInstanceHolder.get();
  }

  private Long userId;
  private Long sellerStoreId;
  private String token;
  private String deviceUuid;
  private String deviceName;

  private Long createTime;
  private Long expireTime;

  public boolean hasExpired() {
    return System.currentTimeMillis()-expireTime>0;
  }

  public Long getUserId() {
    return userId;
  }

  public void setUserId(Long userId) {
    this.userId = userId;
  }

  public Long getSellerStoreId() {
    return sellerStoreId;
  }

  public void setSellerStoreId(Long sellerStoreId) {
    this.sellerStoreId = sellerStoreId;
  }

  public String getToken() {
    return token;
  }

  public void setToken(String token) {
    this.token = token;
  }

  public String getDeviceUuid() {
    return deviceUuid;
  }

  public void setDeviceUuid(String deviceUuid) {
    this.deviceUuid = deviceUuid;
  }

  public String getDeviceName() {
    return deviceName;
  }

  public void setDeviceName(String deviceName) {
    this.deviceName = deviceName;
  }

  public Long getCreateTime() {
    return createTime;
  }

  public void setCreateTime(Long createTime) {
    this.createTime = createTime;
  }

  public Long getExpireTime() {
    return expireTime;
  }

  public void setExpireTime(Long expireTime) {
    this.expireTime = expireTime;
  }
}
