package com.michaels.aggregationgateway.health.controller;

import com.michaels.aggregationgateway.shared.response.RestResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import java.util.LinkedHashMap;
import java.util.Map;

@CrossOrigin
@RestController
@Tag(
    name = "Health Check Controller",
    description = "Check check implementations for CI/CD, used by the K8S liveness probe to ensure that the container is still alive.")
public class HealthController {

  private static final Logger LOGGER = LoggerFactory.getLogger(HealthController.class);

  @Operation(summary = "Ping implementation")
  @ApiResponses(
      value = {
          @ApiResponse(
              responseCode = "200",
              description = "Returned application informations",
              content =@Content(
                  mediaType = "application/json",
                  schema = @Schema(implementation = RestResponse.class)))
      })
  @GetMapping("/ping")
  public Map<String, String> ping() {
    Map<String, String> apiInfo = getApiInfo();
    return apiInfo;
  }

  @Operation(summary = "Health check implementation")
  @ApiResponses(
      value = {
          @ApiResponse(
              responseCode = "200",
              description = "Returned application information and ",
              content =@Content(
                  mediaType = "application/json",
                  schema = @Schema(implementation = RestResponse.class)))
      })
  @GetMapping("/healthcheck")
  public Map<String, Object> healthCheck() {

    Map<String, Object> response = new LinkedHashMap<String, Object>();

    Map<String, String> apiInfo = getApiInfo();
    response.putAll(apiInfo);

    boolean healthCheck = true;
    response.put("healthcheck", convertBooleanToOkFail(healthCheck));
    Map<String, String> resources = new LinkedHashMap<>();
    response.put("resources", resources);

    return response;
  }


  private Map<String, String> getApiInfo() {
    String appProj = System.getenv("APP_PROJ");
    String appEnv = System.getenv("APP_ENV");
    String appName = System.getenv("APP_NAME");
    String appVersion = System.getenv("APP_VERSION");

    Map<String, String> apiInfo=new LinkedHashMap<String, String>();
    apiInfo.put("app_name", String.format("%s-%s-%s", appProj, appEnv, appName));
    apiInfo.put("version", appVersion);

    return  apiInfo;
  }

  private String convertBooleanToOkFail(boolean value) {
    return value?"OK":"FAIL";
  }
}
