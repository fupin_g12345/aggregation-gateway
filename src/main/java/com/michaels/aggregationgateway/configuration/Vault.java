package com.michaels.aggregationgateway.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.vault.annotation.VaultPropertySource;

@Profile("!disableVault")
@Configuration
@VaultPropertySource("${VAULT_ENGINE}/general")
@VaultPropertySource("${VAULT_ENGINE}/spring")
public class Vault {}