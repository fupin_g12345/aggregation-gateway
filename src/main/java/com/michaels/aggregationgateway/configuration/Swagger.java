package com.michaels.aggregationgateway.configuration;

import com.michaels.aggregationgateway.shared.common.BusinessCode;
import io.swagger.v3.core.util.PrimitiveType;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.enums.SecuritySchemeIn;
import io.swagger.v3.oas.annotations.enums.SecuritySchemeType;
import io.swagger.v3.oas.annotations.info.Contact;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.security.SecurityScheme;
import io.swagger.v3.oas.annotations.security.SecuritySchemes;
import io.swagger.v3.oas.annotations.servers.Server;
import io.swagger.v3.oas.models.responses.ApiResponse;
import io.swagger.v3.oas.models.responses.ApiResponses;
import org.springdoc.core.GroupedOpenApi;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@OpenAPIDefinition(
    info =
        @Info(
            title = "Michaels Aggregation Gateway Module",
            version = "v1.0.0",
            description = "Inv Api Documentation",
            contact =
                @Contact(
                    name = "Michaels Tech Team",
                        email = "shenzhen_china@michaelsstores.onmicrosoft.com",
                        url = "https://michaels.atlassian.net/wiki/spaces/DSG/overview?homepageId=495550676")),
    servers = {
            @Server(url = "/api/"),
            @Server(url = "/")
    }
)

@SecuritySchemes({
  @SecurityScheme(
    name = "bearerAuth",
    type = SecuritySchemeType.HTTP,
    in = SecuritySchemeIn.HEADER,
    scheme = "bearer"
  ),
  @SecurityScheme(
    name = "adminBearerAuth",
    type = SecuritySchemeType.APIKEY,
    in = SecuritySchemeIn.HEADER,
    paramName = "Admin-Authorization",
    scheme = "bearer"
  )
})
public class Swagger {

  public Swagger() {
    PrimitiveType.customExcludedClasses().add("java.lang.Long");
    PrimitiveType.customClasses().put("java.lang.Long", PrimitiveType.STRING);
  }

  @Bean
  public GroupedOpenApi healthCheckApi() {
    return GroupedOpenApi.builder()
        .group("HealthCheckApi")
        .packagesToScan("com.michaels.aggregationgateway.health")
        .build();
  }

  private ApiResponses getApiResponses() {
    // status code api response
    ApiResponse _200ApiResponse = new ApiResponse();
    _200ApiResponse.setDescription("OK");

    ApiResponse _400ApiResponse = new ApiResponse();
    _400ApiResponse.setDescription("Request contained invalid data");

    ApiResponse _401ApiResponse = new ApiResponse();
    _401ApiResponse.setDescription("Unauthorized");

    ApiResponse _403ApiResponse = new ApiResponse();
    _403ApiResponse.setDescription("Forbidden");

    ApiResponse _404ApiResponse = new ApiResponse();
    _404ApiResponse.setDescription("Not Found");

    // assemble
    ApiResponses apiResponses=new ApiResponses();
    apiResponses.addApiResponse("200", _200ApiResponse);
    apiResponses.addApiResponse("400", _400ApiResponse);
    apiResponses.addApiResponse("401", _401ApiResponse);
    apiResponses.addApiResponse("403", _403ApiResponse);
    apiResponses.addApiResponse("404", _404ApiResponse);

    // return
    return apiResponses;
  }

  private String getHttpStatusCodeDescription() {
    StringBuilder sb=new StringBuilder();
    sb.append("<h2>Http Status Codes</h2>");
    sb.append("<div style='text-align: left; border: none'>");
    sb.append("<table>");
    sb.append(String.format("<tr><td width='300px'><b>%s</b></td><td><b>%s</b></td></tr>", "Code", "Description"));
    String[][] httpStatusCodeDescs=new String[][]{
        new String[]{"200", "OK"},
        new String[]{"400", "Request contained invalid data"},
        new String[]{"401", "Unauthorized"},
        new String[]{"403", "Forbidden"},
        new String[]{"404", "Not Found"}
    };
    for(String[] httpStatusCodeDesc: httpStatusCodeDescs) {
      String code=httpStatusCodeDesc[0];
      String message=httpStatusCodeDesc[1];
      sb.append(String.format("<tr><td>%s</td><td>%s</td></tr>", code, message));
    }
    sb.append("</div>");
    sb.append("</table>");
    return sb.toString();
  }

  private String getBusinessCodesDescription() {
    StringBuilder sb=new StringBuilder();
    sb.append("<h2>Business Codes</h2>");
    sb.append("<div style='text-align: left; border: none'>");
    sb.append("<table>");
    sb.append(String.format("<tr><td width='300px'><b>%s</b></td><td><b>%s</b></td></tr>", "Code", "Description"));
    BusinessCode[] businessCodes= BusinessCode.class.getEnumConstants();
    for(BusinessCode businessCode: businessCodes) {
      String code=businessCode.getCode();
      String message=businessCode.getMessage();
      sb.append(String.format("<tr><td>%s</td><td>%s</td></tr>", code, message));
    }
    sb.append("</div>");
    sb.append("</table>");
    return sb.toString();
  }

}
