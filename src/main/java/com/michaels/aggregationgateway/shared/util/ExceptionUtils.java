package com.michaels.aggregationgateway.shared.util;

import java.io.PrintWriter;
import java.io.StringWriter;

public class ExceptionUtils {
  public static String getExceptionStackTrace(Throwable t) {
    if(t==null) {
      return "";
    }

    try {
      StringWriter sw = new StringWriter();
      PrintWriter pw = new PrintWriter(sw);
      t.printStackTrace(pw);
      pw.close();
      sw.close();
      return sw.toString();
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }
}
