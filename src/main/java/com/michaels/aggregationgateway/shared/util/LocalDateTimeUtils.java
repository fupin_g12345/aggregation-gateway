package com.michaels.aggregationgateway.shared.util;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;

public class LocalDateTimeUtils {
  public static Long getEpochMillis(LocalDateTime localDateTime) {
    return localDateTime
        .atZone(ZoneId.systemDefault())
        .toInstant()
        .toEpochMilli();
  }

  public static LocalDateTime fromEpochMillis(Long epochMillis) {
    Instant instant = Instant.ofEpochMilli(epochMillis);
    ZoneId zone = ZoneId.systemDefault();
    return LocalDateTime.ofInstant(instant, zone);
  }
}
