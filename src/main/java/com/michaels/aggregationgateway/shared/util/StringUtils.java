package com.michaels.aggregationgateway.shared.util;

import java.util.Collection;

public class StringUtils {

  public static int indexOfIgnoreCase(String s, char c) {
    c = Character.toLowerCase(c);
    for (int i = 0, len = s.length(); i < len; i++) {
      char sc = s.charAt(i);
      if (Character.toLowerCase(sc) == c) {
        return i;
      }
    }
    return -1;
  }

  public static int indexOfIgnoreCase(String s, String c) {
    s = s.toLowerCase();
    c = c.toLowerCase();
    return s.indexOf(c);
  }

  public static boolean isEmpty(String value) {
    if (value == null || value.trim().isEmpty()) {
      return true;
    }
    return false;
  }

  public static String trimWhiteSpace(String value) {
    int start = 0;
    while (start < value.length() && Character.isWhitespace(value.charAt(start))) {
      start++;
    }

    int end = value.length();
    while (end >= start && Character.isWhitespace(value.charAt(end - 1))) {
      end--;
    }
    end = end < start ? start : end;
    end = end > value.length() ? value.length() : end;

    return value.substring(start, end);
  }

  public static boolean startsWith(String value, String prefix, int pos) {

    if (prefix.length() > value.length() - pos) {
      return false;
    }

    for (int i = 0; i < prefix.length(); i++) {
      if (value.charAt(i + pos) != prefix.charAt(i)) {
        return false;
      }
    }

    return true;
  }

  public static boolean equals(String a, String b) {
    if (a == b) {
      return true;
    }
    if (a == null || b == null) {
      return false;
    }

    return a.equals(b);
  }

  public static boolean equalsIgnoreCase(String a, String b) {
    if (a == b) {
      return true;
    }
    if (a == null || b == null) {
      return false;
    }

    return a.equalsIgnoreCase(b);
  }

  public static boolean startsWithIgnoreCase(String value, String prefix) {
    return startsWithIgnoreCase(value, prefix, 0);
  }

  public static boolean startsWithIgnoreCase(String value, String prefix, int pos) {

    if (prefix.length() > value.length() - pos) {
      return false;
    }

    for (int i = 0; i < prefix.length(); i++) {
      if (Character.toLowerCase(value.charAt(i + pos)) != Character.toLowerCase(prefix.charAt(i))) {
        return false;
      }
    }

    return true;
  }

  public static String repeatChar(char c, int count) {
    StringBuilder sb = new StringBuilder();
    for (int i = 0; i < count; i++) {
      sb.append(c);
    }

    return sb.toString();
  }

  public static String nonull(Object value) {
    if (value == null) {
      return "";
    }

    String valueString = value.toString();
    if (valueString == null) {
      valueString = "";
    }
    return valueString;
  }

  public static String join(String[] arr, String separator) {
    StringBuilder sb = new StringBuilder();
    for (int i = 0; i < arr.length; i++) {
      sb.append(arr[i]);
      if (i != arr.length - 1) {
        sb.append(separator);
      }
    }
    return sb.toString();
  }

  public static String join(Collection<String> collection, String separator) {
    StringBuilder sb = new StringBuilder();
    for (String value: collection) {
      if(sb.length()!=0) {
        sb.append(separator);
      }
      sb.append(value);
    }
    return sb.toString();
  }
}
