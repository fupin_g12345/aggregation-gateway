package com.michaels.aggregationgateway.shared.util;

import com.michaels.aggregationgateway.admin.security.auth.AdminAuthContext;
import com.michaels.aggregationgateway.security.auth.AuthContext;

public class UserUtils {
  static public Long getUserId() {
    AdminAuthContext adminAuthContext = AdminAuthContext.get();
    if (adminAuthContext != null) {
      return adminAuthContext.getUserId();
    }

    return AuthContext.get().getUserId();
  }

  static public String getUserIdWithDefault(final String defaultValue) {
    AdminAuthContext adminAuthContext = AdminAuthContext.get();
    if (adminAuthContext != null) {
      return adminAuthContext.getUserId().toString();
    }

    return (AuthContext.get() != null) ? AuthContext.get().getUserId().toString() : defaultValue;
  }
}
