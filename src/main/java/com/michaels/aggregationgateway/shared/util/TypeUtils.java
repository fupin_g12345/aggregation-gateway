/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.michaels.aggregationgateway.shared.util;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author mosmith
 */
public class TypeUtils {

  public static String getString(Object value, String defaultValue) {
    if(value==null) {
      return defaultValue;
    }

    return value.toString();
  }

  public static BigDecimal getDecimal(Object value) {
    if (value == null) {
      return null;
    }

    if (value instanceof BigDecimal) {
      return (BigDecimal) value;
    }
    if (value instanceof BigInteger) {
      return new BigDecimal((BigInteger) value);
    }

    if (value instanceof Short) {
      return new BigDecimal((Short) value);
    }
    if (value instanceof Integer) {
      return new BigDecimal((Integer) value);
    }
    if (value instanceof Float) {
      return new BigDecimal((Float) value);
    }
    if (value instanceof Double) {
      return new BigDecimal((Double) value);
    }

    String valueStr = value.toString();
    try {
      return new BigDecimal(valueStr);
    } catch (Exception e) {
      // e.printStackTrace();
    }

    return null;
  }

  public static Calendar getDate(Object obj) {
    if(obj==null) {
      return null;
    }

    if(obj instanceof Calendar) {
      return (Calendar) obj;
    }

    if(obj instanceof Date) {
      Calendar calendar = Calendar.getInstance();
      calendar.setTime((Date) obj);
      return calendar;
    }

    String str = obj.toString();
    SimpleDateFormat[] innerSupportDateFormats=getInnerSupportSimpleFormats();
    for(SimpleDateFormat dateFormat: innerSupportDateFormats) {
      try {
        Date date = dateFormat.parse(str);
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        return calendar;
      } catch (Exception e) {
        // ignore
      }
    }

    throw new IllegalArgumentException("Unsupport date format: " + str);
  }

  public static boolean equals(Object a, Object b) {
    if(a==b) {
      return true;
    } else if(a!=null && b!=null) {
      return a.equals(b);
    } else {
      return false;
    }
  }

  private static SimpleDateFormat[] getInnerSupportSimpleFormats() {
    SimpleDateFormat[] innerSupportDataFormats = {
        new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"),
        new SimpleDateFormat("yyyy-MM-dd"),
        new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS"),
        new SimpleDateFormat("yyyy-M-d H:m:s"),
        new SimpleDateFormat("yyyy-M-d"),
        new SimpleDateFormat("yyyy-M-d H:m:s.SSS"),

        new SimpleDateFormat("yyyy/MM/dd HH:mm:ss"),
        new SimpleDateFormat("yyyy/MM/dd"),
        new SimpleDateFormat("yyyy/MM/dd HH:mm:ss.SSS"),
        new SimpleDateFormat("yyyy/M/d H:m:s"),
        new SimpleDateFormat("yyyy/M/d"),
        new SimpleDateFormat("yyyy/M/d H:m:s.SSS"),
        new SimpleDateFormat("MM/dd/yyyy HH:mm:ss"),
        new SimpleDateFormat("MM/dd/yyyy"),
        new SimpleDateFormat("MM/dd/yyyy HH:mm:ss.SSS"),
        new SimpleDateFormat("M/d/yyyy H:m:s"),
        new SimpleDateFormat("M/d/yyyy"),
        new SimpleDateFormat("M/d/yyyy H:m:s.SSS"),

        new SimpleDateFormat("HH:mm:ss"),
        new SimpleDateFormat("HH:mm")
    };
    return innerSupportDataFormats;
  }

  public static Boolean getBoolean(Object value, Boolean defaultValue) {
    if(value instanceof Boolean) {
      return (Boolean) value;
    }

    if(value==null) {
      return defaultValue;
    }

    String strValue=value.toString();
    if(strValue.trim().isEmpty()) {
      return defaultValue;
    }

    return Boolean.parseBoolean(strValue);
  }

  public static Integer getInteger(Object value, Integer defaultValue) {
    if(value instanceof Integer) {
      return (Integer)value;
    }

    if(value==null) {
      return defaultValue;
    }

    String strValue=value.toString();
    if(strValue.trim().isEmpty()) {
      return defaultValue;
    }

    return (int) Math.round(Double.parseDouble(strValue));
  }

  public static Long getLong(Object value, Long defaultValue) {
    if(value instanceof Long) {
      return (Long)value;
    }

    if(value==null) {
      return defaultValue;
    }

    String strValue=value.toString();
    if(strValue.trim().isEmpty()) {
      return defaultValue;
    }

    return Long.parseLong(strValue);
  }

  public static Double getDouble(Object value, Double defaultValue) {
    if(value instanceof Double) {
      return (Double) value;
    }

    if(value==null) {
      return defaultValue;
    }

    if(value instanceof Number) {
      return ((Number)value).doubleValue();
    }

    String strValue=value.toString();
    if(strValue.trim().isEmpty()) {
      return defaultValue;
    }

    return Double.parseDouble(strValue);
  }
}
