package com.michaels.aggregationgateway.shared.util;

import java.security.SecureRandom;

public class TokenUtils {
    public static String generateUUIDToken() {
        StringBuilder sb = new StringBuilder();

        for(int i=0;i<6;i++) {
            if(i!=0) {
                sb.append("-");
            }
            SecureRandom secureRandom = new SecureRandom();
            int part=secureRandom.nextInt() & 0xFFFF;
            sb.append(String.format("%04X", part));
        }

        return sb.toString();
    }

    public static String generate6VerifyCode() {
        SecureRandom secureRandom = new SecureRandom();
        int verifyCode=secureRandom.nextInt();
        verifyCode = Math.abs(verifyCode);
        verifyCode = verifyCode % 1000000;

        String code = String.format("%06d", verifyCode);
        return code;
    }
}
