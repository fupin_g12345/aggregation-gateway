package com.michaels.aggregationgateway.shared.util;

import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;

public class IPUtils {

  public static String getIPAddress() {
    HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
    return getIpAddress(request);
  }

  public static String getIpAddress(HttpServletRequest request) {
    String UNKNOWN = "Unknown";
    String[] headers = {
        "X-Forwarded-For",
        "X-Real-IP",
        "Proxy-Client-IP",
        "WL-Proxy-Client-IP",
        "HTTP_X_FORWARDED_FOR",
        "HTTP_X_FORWARDED",
        "HTTP_X_CLUSTER_CLIENT_IP",
        "HTTP_CLIENT_IP",
        "HTTP_FORWARDED_FOR",
        "HTTP_FORWARDED",
        "HTTP_VIA",
        "REMOTE_ADDR"
    };

    String ip = null;
    for(String header: headers) {
      String headerValue = request.getHeader(header);
      if(StringUtils.isEmpty(headerValue) || UNKNOWN.equalsIgnoreCase(headerValue)) {
        continue;
      }

      ip = headerValue;
      if(ip.indexOf(",")>=0) {
        ip = ip.substring(0, ip.indexOf(","));
        ip = ip.trim();
        break;
      }
      if(!StringUtils.isEmpty(ip)) {
        break;
      }
    }

    if(StringUtils.isEmpty(ip)) {
      ip = request.getRemoteAddr();
    }

    if("0:0:0:0:0:0:0:1".equalsIgnoreCase(ip)) {
      ip="127.0.0.1";
    }

    return ip;
  }
}
