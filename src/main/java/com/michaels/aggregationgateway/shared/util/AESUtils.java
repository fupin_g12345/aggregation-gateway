package com.michaels.aggregationgateway.shared.util;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import java.security.SecureRandom;
import java.util.Base64;

public class AESUtils {
  private static final String AES = "AES";

  public static String encrypt(String content, String key) {
    try {
      SecretKey secretKey = generateKey(key);
      Cipher cipher = Cipher.getInstance(AES);
      cipher.init(Cipher.ENCRYPT_MODE, secretKey);
      byte[] encryptedBytes = cipher.doFinal(content.getBytes("UTF-8"));
      String encrypted = Base64.getEncoder().encodeToString(encryptedBytes);
      return encrypted;
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }

  public static String decrypt(String content, String key) {
    try {
      SecretKey secretKey = generateKey(key);
      Cipher cipher = Cipher.getInstance(AES);
      cipher.init(Cipher.DECRYPT_MODE, secretKey);
      byte[] encryptedBytes = Base64.getDecoder().decode(content);
      byte[] decryptedBytes = cipher.doFinal(encryptedBytes);
      String decrypted = new String(decryptedBytes, "UTF-8");
      return decrypted;
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }

  private static SecretKey generateKey(String key) throws Exception {
    SecureRandom random = SecureRandom.getInstance("SHA1PRNG");
    random.setSeed(key.getBytes("UTF-8"));

    KeyGenerator gen = KeyGenerator.getInstance(AES);
    gen.init(128, random);

    return gen.generateKey();
  }

}
