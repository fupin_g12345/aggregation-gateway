package com.michaels.aggregationgateway.shared.validation;

import com.michaels.aggregationgateway.shared.util.StringUtils;

import javax.validation.Constraint;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 *  apply this to request entity validate phone
 */
@Documented
@Target({ElementType.FIELD, ElementType.PARAMETER,ElementType.ANNOTATION_TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = {ValidatePassword.PasswordValidator.class})
public @interface ValidatePassword {
  String message() default "Password does not match the requirements";
  Class<?>[] groups() default {};
  Class<? extends Payload>[] payload() default {};

  public static class PasswordValidator implements ConstraintValidator<ValidatePassword, String> {
    @Override
    public boolean isValid(String password, ConstraintValidatorContext constraintValidatorContext) {
      if (StringUtils.isEmpty(password)) {
        return true;
      }
      return password.matches("(.{6,30})")
          && password.matches("(.*[A-Z]+.*)")
          && password.matches("(.*[a-z]+.*)")
          && password.matches("(.*[0-9]+.*)");
    }
  }
}
