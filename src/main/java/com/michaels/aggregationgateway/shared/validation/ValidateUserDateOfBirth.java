package com.michaels.aggregationgateway.shared.validation;

import com.michaels.aggregationgateway.shared.util.StringUtils;

import javax.validation.Constraint;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import javax.validation.Payload;
import java.lang.annotation.*;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 *  apply this to request entity validate phone
 */
@Documented
@Target({ElementType.FIELD, ElementType.PARAMETER,ElementType.ANNOTATION_TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = {ValidateUserDateOfBirth.UserDateOfBirthValidator.class})
public @interface ValidateUserDateOfBirth {
  String message() default "Invalid date of birth or user is not yet 13 years old";
  Class<?>[] groups() default {};
  Class<? extends Payload>[] payload() default {};

  public static class UserDateOfBirthValidator implements ConstraintValidator<ValidateUserDateOfBirth, String> {
    private static final int SIGNUP_QUALIFIED_AGE = 13;
    private SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM/dd/yyyy");

    @Override
    public boolean isValid(String dateOfBirth, ConstraintValidatorContext constraintValidatorContext) {
      if(StringUtils.isEmpty(dateOfBirth)) {
        return true;
      }
      try {
        Date date = simpleDateFormat.parse(dateOfBirth);
        Calendar dobCalendar = Calendar.getInstance();
        dobCalendar.setTime(date);

        Calendar qualifiedCalendar = Calendar.getInstance();
        qualifiedCalendar.add(Calendar.YEAR, -SIGNUP_QUALIFIED_AGE);

        if(qualifiedCalendar.before(dobCalendar)) {
          return false;
        }
        return true;

      } catch (Exception e) {
        return false;
      }
    }
  }
}
