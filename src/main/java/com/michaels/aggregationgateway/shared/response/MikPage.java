package com.michaels.aggregationgateway.shared.response;

import lombok.Data;

import java.util.List;

@Data
public class MikPage<T> {
  private List<T> content;
  private Integer pageNum;
  private Integer pageSize;
  private Integer totalPages;
}
