package com.michaels.aggregationgateway.shared.response;

import lombok.Data;
import org.springframework.http.HttpStatus;

import java.util.Date;

@Data
public class ErrorResponse {

  private final HttpStatus status;
  private final String stackTrace;
  private String origin;
  private String timestamp;

  public ErrorResponse(final HttpStatus status, final Exception ex) {
    this.status = status;
    this.stackTrace = ex.getMessage();
    origin = ex.getClass().getSimpleName();
    timestamp = new Date().toString();
  }
}