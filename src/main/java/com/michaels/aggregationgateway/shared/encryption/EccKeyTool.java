package com.michaels.aggregationgateway.shared.encryption;

import java.security.*;
import java.security.interfaces.ECPrivateKey;
import java.security.interfaces.ECPublicKey;
import java.util.HashMap;
import java.util.Map;
/**
 * @author fswan
 * @since 2021-01-21 9:47 AM
 * Use to generate ecc key pair.
 */
public class EccKeyTool {
    public static final int KEY_SIZE = 256;
    public static final String ALGORITHM ="EC";
    public static final String PROVIDER = "BC";
    public static final String PUBLIC_KEY = "PUBLIC_KEY";
    public static final String PRIVATE_KEY = "PRIVATE_KEY";

    static {
        Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
    }

    /**
     * Get generate key
     *
     * @return a map contain ecc key pair
     * @throws NoSuchProviderException exception
     * @throws NoSuchAlgorithmException  exception
     */
    public static Map<String, byte[]> getGenerateKey() throws NoSuchProviderException, NoSuchAlgorithmException {
        KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance(ALGORITHM,
                PROVIDER);
        keyPairGenerator.initialize(KEY_SIZE, new SecureRandom());
        KeyPair kp = keyPairGenerator.generateKeyPair();
        ECPublicKey publicKey = (ECPublicKey) kp.getPublic();
        ECPrivateKey privateKey = (ECPrivateKey) kp.getPrivate();
        Map<String, byte[]> map = new HashMap<>();
        map.put(PRIVATE_KEY, privateKey.getEncoded());
        map.put(PUBLIC_KEY, publicKey.getEncoded());
        return map;
    }

}
