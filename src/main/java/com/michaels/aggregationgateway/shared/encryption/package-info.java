/**
 * @author fswan
 * @since 2021-01-21 9:47 AM
 *
 * This is a encryption tool.
 *
 * {@link com.michaels.aggregationgateway.shared.encryption.EncryptTool#encrypt(java.lang.String, byte[], byte[])} used to encrypt plain text.
 * {@link com.michaels.aggregationgateway.shared.encryption.EncryptTool#decrypt(java.lang.String, byte[])} used to decrypt text.
 *
 * You can use {@link com.michaels.aggregationgateway.shared.encryption.EncryptTool#getEccKeyPair()} to gent ECc key pair.
 *
 * A dependency is need to be add to pom.xml
 * <dependency>
 *             <groupId>org.bouncycastle</groupId>
 *             <artifactId>bcprov-jdk15on</artifactId>
 *             <version>1.56</version>
 * </dependency>
 */
package com.michaels.aggregationgateway.shared.encryption;