
package com.michaels.aggregationgateway.shared.encryption;

import javax.crypto.*;
import java.nio.charset.StandardCharsets;
import java.security.*;
import java.security.interfaces.ECPrivateKey;
import java.security.interfaces.ECPublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;
import java.util.Map;
/**
 * @author fswan
 * @since 2021-01-21 9:47 AM
 */
public class EncryptTool {

    public static final char SPLIT_CHARACTER = ',';
    public static final int KEY_SIZE = EccKeyTool.KEY_SIZE;
    public static final String ALGORITHM = "AES";
    public static final String RNG_ALGORITHM = "SHA1PRNG";

    /**
     * Generate AES random key
     *
     * @return aes key
     * @throws NoSuchAlgorithmException Algorithm exception
     */
    public static byte[] getAesRandomKey() throws NoSuchAlgorithmException {
        KeyGenerator keygen = KeyGenerator.getInstance(ALGORITHM);
        SecretKey deskey = keygen.generateKey();
        return deskey.getEncoded();
    }

    /**
     * AES encrypt
     * @param plainBytes bytes need to be encrypt.
     * @param key aes key
     * @return bytes which is encrypted.
     */
    public static byte[] aesEncrypt(byte[] plainBytes, byte[] key) throws NoSuchAlgorithmException, NoSuchPaddingException, BadPaddingException, IllegalBlockSizeException, InvalidKeyException {
        SecretKey secKey = generateAESKey(key);
        Cipher cipher = Cipher.getInstance(ALGORITHM);
        cipher.init(Cipher.ENCRYPT_MODE, secKey);
        byte[] cipherBytes = cipher.doFinal(plainBytes);
        return cipherBytes;
    }

    /**
     * AES decrypt
     *
     * @param cipherBytes bytes need to be decrypt.
     * @param key aes key
     * @return origin bytes
     */
    public static byte[] aesDecrypt(byte[] cipherBytes, byte[] key) throws NoSuchPaddingException, NoSuchAlgorithmException, BadPaddingException, IllegalBlockSizeException, InvalidKeyException {
        SecretKey secKey = generateAESKey(key);
        Cipher cipher = Cipher.getInstance(ALGORITHM);
        cipher.init(Cipher.DECRYPT_MODE, secKey);
        byte[] plainBytes = cipher.doFinal(cipherBytes);
        return plainBytes;
    }

    /**
     * Convert byte array to Secret key.
     * @param key byte array.
     * @return secret key.
     */
    private static SecretKey generateAESKey(byte[] key) throws NoSuchAlgorithmException {
        SecureRandom random = SecureRandom.getInstance(RNG_ALGORITHM);
        random.setSeed(key);
        KeyGenerator gen = KeyGenerator.getInstance(ALGORITHM);
        gen.init(KEY_SIZE, random);
        return gen.generateKey();
    }

    /**
     * Get ECC key pair
     * @return ecc key pair.
     * @throws NoSuchProviderException
     * @throws NoSuchAlgorithmException
     * @see EccKeyTool#PUBLIC_KEY  public key
     * @see EccKeyTool#PRIVATE_KEY private key
     */
    public static Map<String, byte[]> getEccKeyPair() throws NoSuchProviderException, NoSuchAlgorithmException {
        return EccKeyTool.getGenerateKey();
    }

    /**
     * ECC encrypt
     * @param data bytes need to be encrypt.
     * @param publicKey ecc public key
     * @return byte array which is encrypted.
     */
    public static byte[] eccEncrypt(byte[] data, byte[] publicKey) throws NoSuchAlgorithmException, InvalidKeySpecException, BadPaddingException, IllegalBlockSizeException, InvalidKeyException {
        byte[] keyBytes = publicKey;
        X509EncodedKeySpec x509KeySpec = new X509EncodedKeySpec(keyBytes);
        KeyFactory keyFactory = KeyFactory.getInstance(EccKeyTool.ALGORITHM);
        ECPublicKey pubKey = (ECPublicKey) keyFactory.generatePublic(x509KeySpec);
        Cipher cipher = new NullCipher();
        cipher.init(Cipher.ENCRYPT_MODE, pubKey);
        return cipher.doFinal(data);
    }

    /**
     * ECC decrypt
     * @param data byte array nedd to be decrypt
     * @param privateKey Ecc private key
     * @return origin byte array.
     */
    public static byte[] eccDecrypt(byte[] data, byte[] privateKey) throws InvalidKeySpecException, NoSuchAlgorithmException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException {
        byte[] keyBytes = privateKey;
        PKCS8EncodedKeySpec pkcs8KeySpec = new PKCS8EncodedKeySpec(keyBytes);
        KeyFactory keyFactory = KeyFactory.getInstance(EccKeyTool.ALGORITHM);
        ECPrivateKey priKey = (ECPrivateKey) keyFactory.generatePrivate(pkcs8KeySpec);
        Cipher cipher = new NullCipher();
        cipher.init(Cipher.DECRYPT_MODE, priKey);
        return cipher.doFinal(data);
    }

    /**
     * encrypt text and aes key to a string with ecc.
     *
     * @param plainText    plain text which is need to be encrypted
     * @param aesKey       aes key, which is used to encrypt plain text ,and to be encrypted with ecc.
     * @param eccPublicKey ecc key ,use to encrypt aes key
     * @return
     */
    public static final String encrypt(String plainText, byte[] aesKey, byte[] eccPublicKey) {
        try {
            byte[] encryptedText = aesEncrypt(plainText.getBytes(StandardCharsets.UTF_8), aesKey);
            byte[] encryptedKey = eccEncrypt(aesKey, eccPublicKey);
            return Base64.getEncoder().encodeToString(encryptedKey) + SPLIT_CHARACTER + Base64.getEncoder().encodeToString(encryptedText);
        }catch (NoSuchPaddingException | NoSuchAlgorithmException | BadPaddingException | IllegalBlockSizeException | InvalidKeyException | InvalidKeySpecException e) {
            throw new EncryptException(e);
        }
    }

    /**
     * decrypt text which combine aes key and content.
     *
     * @param encryptedText encrypted text, it's join aes key with content.
     * @param eccPrivateKey ecc private key ,use to decrypt aes key.
     * @return plain text
     */
    public static final String decrypt(String encryptedText, byte[] eccPrivateKey) {
        if (encryptedText.indexOf(SPLIT_CHARACTER) == -1)
            throw new RuntimeException("Text format is not correct.");
        byte[] aesKey = Base64.getDecoder().decode(encryptedText.substring(0, encryptedText.indexOf(SPLIT_CHARACTER)));
        byte[] encryptedContent = Base64.getDecoder().decode(encryptedText.substring(encryptedText.indexOf(SPLIT_CHARACTER) + 1));
        try {
            return new String(aesDecrypt(encryptedContent, aesKey), StandardCharsets.UTF_8);
        } catch (NoSuchPaddingException | NoSuchAlgorithmException | BadPaddingException | IllegalBlockSizeException | InvalidKeyException e) {
            throw new EncryptException(e);
        }
    }

    public static void main(String[] args) throws Exception {
        Map<String, byte[]> eccKeyPaire = EncryptTool.getEccKeyPair();
        byte[] aesKey = EncryptTool.getAesRandomKey();
        String decrypted = EncryptTool.encrypt("good morning", aesKey, eccKeyPaire.get(EccKeyTool.PUBLIC_KEY));
        System.out.println(decrypted);
        String original = EncryptTool.decrypt(decrypted, eccKeyPaire.get(EccKeyTool.PRIVATE_KEY));
        System.out.println(original);
    }
}
