Support function：

1、Generate ECC key pair (public key and private key)。

2、Generate random AES key。

3、ECC encrypt and decrypt 。

4、AES decrypt and encrypt.

Configurion：

1、Encrypt Side（encrypt data）: Load ECC private key from Vault. 

​			When service start , generate a random AES256 key and encrypt AES key with ECC private key.

​			When a text need to be encrypt , we use AES key encrypt the text and send encrypted text and encrypted AES key , and save it in DB.

2、Decrypt side（decrypt data）:Load ECC public key from Vault. 

​		We load encrypted text and key from db , and then decrypt aes key with ecc public key (Cache it ), and use AES key decrypt text.



```mermaid
sequenceDiagram
alt start service
Encrypt Side ->> Encrypt Side: Generate random AES256 key
Encrypt Side ->> Encrypt Side: ECC private key encrypt AES key
end

alt encrypt
Encrypt Side ->> DB : Encrypted text with aes key and encrypted aes key with ecc
end

alt decrypt
Decrypt Side ->> DB:Get encrypted text and encrypted aes key
Decrypt Side ->> Decrypt Side: Decrypt aes key
Decrypt Side ->> Decrypt Side: Decrypt text with aes key
end

```

