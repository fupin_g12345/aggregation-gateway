package com.michaels.aggregationgateway.shared.encryption;

/**
 * @author swan
 * @since 2021-01-21 1:29 PM
 * Encrypt exception used to avoid encrypt and decrypt method need to process exception.
 */
public class EncryptException extends RuntimeException {
    public EncryptException(Exception ex){
        super(ex);
    }
}
