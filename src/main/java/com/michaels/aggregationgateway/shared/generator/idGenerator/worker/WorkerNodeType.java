package com.michaels.aggregationgateway.shared.generator.idGenerator.worker;

import com.michaels.aggregationgateway.shared.generator.idGenerator.utils.ValuedEnum;

/**
 * WorkerNodeType
 * <li>CONTAINER: Such as Docker
 * <li>ACTUAL: Actual machine
 */
public enum WorkerNodeType implements ValuedEnum<Integer> {
  CONTAINER(1),
  ACTUAL(2);

  /** Lock type */
  private final Integer type;

  /** Constructor with field of type */
  WorkerNodeType(Integer type) {
    this.type = type;
  }

  @Override
  public Integer value() {
    return type;
  }
}
