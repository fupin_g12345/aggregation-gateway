package com.michaels.aggregationgateway.shared.generator.idGenerator.worker;

public interface WorkerIdAssigner {

  /**
   * Assign worker id for
   *
   * @return assigned worker id
   */
  long assignWorkerId(long max);
}
