package com.michaels.aggregationgateway.shared.generator.idGenerator.worker;

import com.michaels.aggregationgateway.shared.generator.idGenerator.utils.DockerUtils;
import com.michaels.aggregationgateway.shared.generator.idGenerator.utils.NetUtils;
import org.apache.commons.lang3.RandomUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DisposableWorkerIdAssigner implements WorkerIdAssigner {

  private static final Logger LOGGER = LoggerFactory.getLogger(DisposableWorkerIdAssigner.class);

  @Override
  public long assignWorkerId(long max) {
    WorkerNodeEntity workerNodeEntity = buildWorkerNode();
    return getHostId(workerNodeEntity.getHostName(), max);
  }

  /** get the host id */
  private long getHostId(String s, long max) {
    byte[] bytes = s.getBytes();
    int sums = 0;
    for (int b : bytes) {
      sums += b;
    }
    return sums % (max + 1);
  }

  /** Build worker node entity by IP and PORT */
  private WorkerNodeEntity buildWorkerNode() {
    WorkerNodeEntity workerNodeEntity = new WorkerNodeEntity();
    if (DockerUtils.isDocker()) {
      workerNodeEntity.setType(WorkerNodeType.CONTAINER.value());
      workerNodeEntity.setHostName(DockerUtils.getDockerHost());
      workerNodeEntity.setPort(DockerUtils.getDockerPort());

    } else {
      workerNodeEntity.setType(WorkerNodeType.ACTUAL.value());
      workerNodeEntity.setHostName(NetUtils.getLocalAddress());
      workerNodeEntity.setPort(System.currentTimeMillis() + "-" + RandomUtils.nextInt());
    }

    return workerNodeEntity;
  }
}
