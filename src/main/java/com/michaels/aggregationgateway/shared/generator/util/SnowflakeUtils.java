package com.michaels.aggregationgateway.shared.generator.util;

import java.net.Inet4Address;
import java.net.UnknownHostException;
import java.util.Random;

public class SnowflakeUtils {

  /** time bits */
  private static final int TIME_LEN = 41;
  /** data center bits */
  private static final int DATA_LEN = 2;
  /** worker bits */
  private static final int WORK_LEN = 8;
  /** sequence bits */
  private static final int SEQ_LEN = 12;

  /** start time 2020-08-01 00:00:00 */
  private static final long START_TIME = 1596211200000L;
  /** last timestamp */
  private static long LAST_TIME_STAMP = -1L;
  /** left bits */
  private static final int TIME_LEFT_BIT = 64 - 1 - TIME_LEN;

  /** get the data center id */
  private static final long DATA_ID = getDataId();
  /** get the worker id */
  private static final long WORK_ID = getWorkId();
  /** max numbers for data center */
  private static final int DATA_MAX_NUM = ~(-1 << DATA_LEN);
  /** max numbers for worker id */
  private static final int WORK_MAX_NUM = ~(-1 << WORK_LEN);
  /** get random datacenter id */
  private static final int DATA_RANDOM = DATA_MAX_NUM + 1;
  /** get random worker id */
  private static final int WORK_RANDOM = WORK_MAX_NUM + 1;
  /** data center left bits */
  private static final int DATA_LEFT_BIT = TIME_LEFT_BIT - DATA_LEN;
  /** worker id left bits */
  private static final int WORK_LEFT_BIT = DATA_LEFT_BIT - WORK_LEN;

  /** last sequence */
  private static long LAST_SEQ = 0L;
  /** max sequence */
  private static final long SEQ_MAX_NUM = ~(-1 << SEQ_LEN);

  public static synchronized long genId() {
    long now = System.currentTimeMillis();

    if (now < LAST_TIME_STAMP) {
      throw new RuntimeException(
          String.format("Clock moved backwards！%d seconds", START_TIME - now));
    }

    if (now == LAST_TIME_STAMP) {
      LAST_SEQ = (LAST_SEQ + 1) & SEQ_MAX_NUM;
      if (LAST_SEQ == 0) {
        now = nextMillis(LAST_TIME_STAMP);
      }
    } else {
      LAST_SEQ = 0;
    }

    LAST_TIME_STAMP = now;

    return ((now - START_TIME) << TIME_LEFT_BIT)
        | (DATA_ID << DATA_LEFT_BIT)
        | (WORK_ID << WORK_LEFT_BIT)
        | LAST_SEQ;
  }

  /** get the timestamp */
  public static long nextMillis(long lastMillis) {
    long now = System.currentTimeMillis();
    while (now <= lastMillis) {
      now = System.currentTimeMillis();
    }
    return now;
  }

  /** get the host id */
  private static int getHostId(String s, int max) {
    byte[] bytes = s.getBytes();
    int sums = 0;
    for (int b : bytes) {
      sums += b;
    }
    return sums % (max + 1);
  }

  /** get the worker id */
  public static int getWorkId() {
    try {
      return getHostId(Inet4Address.getLocalHost().getHostAddress(), WORK_MAX_NUM);
    } catch (UnknownHostException e) {
      return new Random().nextInt(WORK_RANDOM);
    }
  }

  /** get the datacenter id */
  public static int getDataId() {
    try {
      return getHostId(Inet4Address.getLocalHost().getHostName(), DATA_MAX_NUM);
    } catch (UnknownHostException e) {
      return new Random().nextInt(DATA_RANDOM);
    }
  }
}
