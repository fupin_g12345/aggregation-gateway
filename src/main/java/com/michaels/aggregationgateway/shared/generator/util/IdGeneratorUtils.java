package com.michaels.aggregationgateway.shared.generator.util;

import com.michaels.aggregationgateway.shared.generator.idGenerator.exception.UidGenerateException;
import com.michaels.aggregationgateway.shared.generator.idGenerator.impl.DefaultUidGenerator;
import com.michaels.aggregationgateway.shared.generator.idGenerator.worker.DisposableWorkerIdAssigner;
import com.michaels.aggregationgateway.shared.generator.idGenerator.worker.WorkerIdAssigner;

public final class IdGeneratorUtils {
  private static DefaultUidGenerator uidGenerator = null;

  private static void init() {
    if (uidGenerator == null) {
      uidGenerator = new DefaultUidGenerator();
      WorkerIdAssigner workerIdAssigner = new DisposableWorkerIdAssigner();
      uidGenerator.setWorkerIdAssigner(workerIdAssigner);
      uidGenerator.afterPropertiesSet();
    }
  }

  /*
   * generate an unique long id
   * */
  public static Long getUID() {
    try {
      if (uidGenerator == null) {
        init();
      }
      return uidGenerator.getUID();
    } catch (Exception e) {
      throw new UidGenerateException();
    }
  }

  private IdGeneratorUtils() {}
}
