package com.michaels.aggregationgateway.shared.exceptionhandler;

import java.util.Map;
import java.util.stream.Collectors;

import com.michaels.aggregationgateway.shared.common.BusinessCode;
import com.michaels.aggregationgateway.shared.exception.MikException;
import com.michaels.aggregationgateway.shared.exception.UserLockedException;
import com.michaels.aggregationgateway.shared.response.RestResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class GlobalExceptionHandler {
    private static final Logger LOGGER = LoggerFactory.getLogger(GlobalExceptionHandler.class);

    @ResponseBody
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<Object> handleMethodArgumentNotValidException(final MethodArgumentNotValidException ex) {
        String message = ex.getBindingResult().getAllErrors().stream()
            .map(DefaultMessageSourceResolvable::getDefaultMessage)
            .collect(Collectors.joining());
        RestResponse<Object> messageResponse = new RestResponse<>(BusinessCode.MCU_ARGUMENT_NOT_VALID.getCode(), message, null);
        return new ResponseEntity<>(messageResponse, new HttpHeaders(), HttpStatus.BAD_REQUEST);
    }

    @ResponseBody
    @ExceptionHandler(UserLockedException.class)
    public ResponseEntity<Object> handleUserLockedException(final UserLockedException ex) {
        LOGGER.error("UserLockedException: ", ex);
        RestResponse messageResponse = RestResponse.fail(ex);
        messageResponse.setData(Map.of("unlockTimeMillis", ex.getUnlockTimeInMillis()));
        return new ResponseEntity<>(messageResponse, new HttpHeaders(), ex.getStatus());
    }

    @ResponseBody
    @ExceptionHandler(MikException.class)
    public ResponseEntity<Object> handleMikException(final MikException ex) {
        LOGGER.error("MikException: ", ex);
        RestResponse messageResponse = RestResponse.fail(ex);
        return new ResponseEntity<>(messageResponse, new HttpHeaders(), ex.getStatus());
    }
}
