package com.michaels.aggregationgateway.shared.common;

public enum BusinessCode {
    MCU_200("200", "OK"),

    MCU_INTERNAL_ERROR("MCU_INTERNAL_ERROR", "Remote service is not available"),
    MCU_STORE_CANNOT_MODIFY("MCU_STORE_CANNOT_MODIFY", "Store info cann't modify"),
    MCU_UESR_HAVE_NO_STORE_PROFILE("MCU_USER_HAVE_NOT_STORE_PROFILE", "User doesn't have store profile"),
    MCU_BAD_PHASE("MCU_BAD_PHASE","Error phase type"),
    MCU_STORE_NAME_NEEDED("MCU_STORENAME_NEEDED", "Store name is not provided"),
    MCU_WALLET_BANK_CARD_CREATE_FAILED("MCU_WALLET_BANK_CARD_CREATE_FAILED", "Failed to create bank card"),
    MCU_WALLET_BANK_CARD_UP_TO_LIMIT("MCU_WALLET_BANK_CARD_UP_TO_LIMIT", "Band card up to limit"),
    MCU_STORE_INVALID_SELLER_PLAN("MCU_STORE_INVALID_SELLER_PLAN", "Store invalid seller plan"),
    MCU_STORE_EXIST_SELLER_PLAN("MCU_STORE_EXIST_SELLER_PLAN", "Store exist seller plan"),
    MCU_STORE_NAME_USED("MCU_STORE_NAME_USED", "Store name has been used"),
    MCU_DISPLAY_USED("MCU_DISPLAY_USED", "Display name has been used"),
    MCU_INAPPROPRIATE("MCU_INAPPROPRIATE",  "Contains inappropriate content"),

    MCU_ARGUMENT_NOT_VALID("MCU_ARGUMENT_NOT_VALID", "Argument is not valid"),
    MCU_PASSWORD_FORMAT_INELIGIBLE("MCU_PASSWORD_FORMAT_INELIGIBLE", "Password does not match the format"),
    MCU_USER_NOT_FOUND("MCU_USER_NOT_FOUND", "User not found with given email"),
    MCU_EMAIL_ALREADY_IN_USE("MCU_EMAIL_ALREADY_IN_USE", "User already exists with this email address"),
    MCU_EMAIL_GUEST_USER("MCU_EMAIL_GUEST_USER", "User already used by guest checkout"),
    MCU_PASSWORD_INCORRECT("MCU_PASSWORD_INCORRECT", "Incorrect password"),
    MCU_EMAIL_PASSWORD_INCORRECT("MCU_EMAIL_PASSWORD_INCORRECT", "Email or password incorrect"),
    MCU_NEW_PASSWORD_CANNOT_BE_SAME_AS_THE_OLD("MCU_NEW_PASSWORD_CANNOT_BE_SAME_AS_THE_OLD", "New password cannot be same as the old password"),
    MCU_INVALID_TOKEN("MCU_INVALID_TOKEN", "Invalid token"),
    MCU_INVALID_JWT("MCU_INVALID_JWT", "Invalid JWT token"),
    MCU_INVALID_ACCOUNT_STAUTS("MCU_INVALID_ACCOUNT_STAUTS", "Invalid account status value"),
    MCU_TOKEN_NOT_FOUND("MCU_TOKEN_NOT_FOUND", "User token not found"),
    MCU_USER_NOT_CONFIRMED("MCU_USER_NOT_CONFIRMED", "User account was not confirmed, please confirm the account first"),
    MCU_USER_INACTIVE("MCU_USER_INACTIVE", "User was inactived"),
    MCU_USER_LOCKED("MCU_USER_LOCKED", "User was locked"),
    MCU_UNAUTHORIZED_ACCESS("MCU_UNAUTHORIZED_ACCESS", "Unauthorized access"),
    MCU_ADMIN_UNAUTHORIZED_ACCESS("MCU_ADMIN_UNAUTHORIZED_ACCESS", "admin Unauthorized access"),
    MCU_ADMIN_AUTHORIZED_PARAM_ERROR("MCU_ADMIN_AUTHORIZED_PARAM_ERROR", "admin authorized param error"),
    MCU_LOGIN_RETRY_TIMES_EXCEED("MCU_LOGIN_RETRY_TIMES_EXCEED", "Account is locked for the login retry times exceeds the limit, please try later!"),
    MCU_SWAP_EMAIL_CANNOT_BE_SAME("MCU_SWAP_EMAIL_CANNOT_BE_SAME", "New email cannot be same as the old email"),

    MCU_DEVICE_CANNOT_DELETE_CURRENT("MCU_DEVICE_CANNOT_DELETE_CURRENT", "Cannot delete current device"),
    MCU_DEVICE_NOT_FOUND("MCU_DEVICE_NOT_FOUND", "Device not found"),
    MCU_DEVICE_SESSION_NOT_FOUND("MCU_DEVICE_SESSION_NOT_FOUND", "Device session not found!"),

    MCU_ADDRESS_NOT_FOUND("MCU_ADDRESS_NOT_FOUND", "Address not found"),
    MCU_ADDRESS_USPS_VERIFY_ERROR("MCU_ADDRESS_USPS_VERIFY_ERROR", "USPS failed to validate this address"),

    MCU_USER_TOKEN_INVALID_TOKEN_TYPE("MCU_INVALID_TOKEN_TYPE", "Invalid token type, token type for non-user module must be large than 10000"),
    MCU_USER_ALREADY_CONFIRMED("MCU_USER_ALREADY_CONFIRMED", "User account has already been confirmed"),

    MCU_USER_STATE_TAX_EXEMPT_INFORMATION_NOT_FOUND("", "User state tax exempt information not found with given id"),

    MCU_DICT_ITEM_NOT_FOUND("MCU_DICT_ITEM_NOT_FOUND", "Dictionary item not found with given id"),
    MCU_DICT_PARENT_ID_NOT_FOUND("MCU_DICT_PARENT_ID_NOT_FOUND", "Parent dictionary item not found with given id"),
    MCU_DICT_ITEM_ALREADY_EXIST("MCU_DICT_ITEM_ALREADY_EXIST", "Dictionary item already exists with given code and parent id"),

    MCU_SEARCH_HISTORY_NOT_FOUND("MCU_SEARCH_HISTORY_NOT_FOUND", "Item not found with given id"),
    MCU_SEARCH_HISTORY_INVALID_CHANNEL("MCU_SEARCH_HISTORY_INVALID_CHANNEL", "Invalid channel value"),
    MCU_SEARCH_HISTORY_INVALID_SEARCH_TYPE("MCU_SEARCH_HISTORY_INVALID_SEARCH_TYPE", "Invalid search type value"),

    MCU_THIRD_PARTY_ACCOUNT_ALREADY_BOUND("MCU_THIRD_PARTY_ACCOUNT_ALREADY_BOUND", "Social media account already bound"),
    MCU_THIRD_PARTY_ACCOUNT_NOT_BOUND("MCU_THIRD_PARTY_ACCOUNT_NOT_BOUND", "Social media account not bound"),
    MCU_THIRD_PARTY_INVALID_TOKEN("MCU_THIRD_PARTY_INVALID_TOKEN", "Invalid social media account token"),
    MCU_THIRD_PARTY_ACCOUNT_NOT_FOUND("MCU_THIRD_PARTY_ACCOUNT_NOT_FOUND", "Social media account not found"),


    //Inventory
    MCU_INVENTORY_NOT_ENOUGH("MCU_12000", "Inventory does not enough"),
    MCU_INVENTORY_REDUCE_FAILED("MCU_12001", "Inventory reduce failed"),
    MCU_INVENTORY_ORDER_HAD_REDUCED("MCU_12002", "Inventory had reduced by this order"),
    MCU_INVENTORY_ORDER_NOT_EXISTED("MCU_12003", "The order of inventory is not existed"),
    MCU_INVENTORY_RETURN_FAILED("MCU_12004", "Inventory return failed"),
    MCU_INVENTORY_RETURN_CANCELED_MORE_THAN_ORDER("MCU_12005", "Total return or cancel inventory can not more than place order inventory"),
    MCU_INVENTORY_OMNI_PRODUCT_NOT_EXISTED("MCU_12006", "Omni product not existed"),
    MCU_INVENTORY_JSON_PARSE_ERROR("MCU_12007", "Json parse error"),
    MCU_INVENTORY_OMNI_RESERVATION_FAILED("MCU_12008", "Omni inventory reservation failed"),
    MCU_INVENTORY_OMNI_DELETE_RESERVATION_FAILED("MCU_12009", "Omni inventory delete reservation failed"),
    MCU_INVENTORY_OMNI_REDUCE_NOT_SUPPORT("MCU_12010", "Omni inventory omni reduce not support"),
    MCU_INVENTORY_OMNI_RETURN_NOT_SUPPORT("MCU_12011", "Omni inventory omin return not support"),
    MCU_INVENTORY_RESERVATION_EXPIREDTIME_ERROR("MCU_12012", "reservation expired time error"),
    MCU_INVENTORY_RESERVATION_HAD_DELETED("MCU_12013", "Inventory reservation had deleted"),
    MCU_INVENTORY_NOT_EXIST("MCU_12014", "Inventory does not exist"),
    MCU_INVENTORY_UN_RESERVATION_FAILED("MCU_12015", "Inventory un-reservation failed"),
    MCU_INVENTORY_ORDER_HAD_RESERVATION("MCU_12016", "Inventory had reservation by this order"),
    MCU_INVENTORY_RESERVATION_FAILED("MCU_12017", "Inventory reservation failed"),
    MCU_INVENTORY_MANHATTAN_API_ACCESS_FAILED("MCU_12018", "Inventory manhattan api access failed"),
    MCU_INVENTORY_NOT_RESERVATION("MCU_12019", "Inventory not reservation"),
    MCU_INVENTORY_RESERVATION_REDUCE_NOT_SAME("MCU_12020", "Inventory reservation and reduce not same"),
    MCU_INVENTORY_SKU_EXISTED("MCU_12021", "Inventory sku existed"),
    MCU_INVENTORY_SKU_SUBSCRIBE_EXISTED("MCU_12022", "Inventory sku subscribe existed"),
    MCU_INVENTORY_SKU_NO_AUTHORITITY("MCU_12023", "Sku Inventory no authoritity"),

    //Price
    MCU_PRICE_NOT_EXIST("MCU_13000", "Sku Price does not exist"),
    MCU_PRICE_RULE_NOT_EXIST("MCU_13001", "Sku Price rule does not exist"),
    MCU_PRICE_SKU_NO_AUTHORITITY("MCU_13002", "Sku Price no authoritity"),
    MCU_PRICE_RULE_SKU_PRICE_CONFLICT("MCU_13002", "Sku Price Rule price conflicted ");

    private final String code;
    private final String message;

    @Deprecated
    BusinessCode(String code) {
        this.code = code;
        this.message = "";
    }

    BusinessCode(String code, String message){
        this.code = code;
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public String getMessage(){
        return message;
    }
}
