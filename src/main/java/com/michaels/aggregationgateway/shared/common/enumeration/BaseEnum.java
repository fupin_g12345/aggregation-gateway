package com.michaels.aggregationgateway.shared.common.enumeration;

public interface BaseEnum<T> {
  public int getIntValue();
  public String getStringValue();
}
