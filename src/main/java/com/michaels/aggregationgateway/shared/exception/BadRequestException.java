package com.michaels.aggregationgateway.shared.exception;

import com.michaels.aggregationgateway.shared.common.BusinessCode;
import org.springframework.http.HttpStatus;

public class BadRequestException extends MikException {
  public BadRequestException(BusinessCode bc, String message) {
    super(HttpStatus.BAD_REQUEST, bc, message);
  }

  public BadRequestException(BusinessCode bc) {

    super(HttpStatus.BAD_REQUEST, bc);

  }
}
