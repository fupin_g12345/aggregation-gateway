package com.michaels.aggregationgateway.shared.exception;

import com.michaels.aggregationgateway.shared.common.BusinessCode;
import org.springframework.http.HttpStatus;

public class OrderException extends  MikException{


    public OrderException(BusinessCode bc) {
        super(HttpStatus.BAD_REQUEST, bc);
    }

}
