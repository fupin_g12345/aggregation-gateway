package com.michaels.aggregationgateway.shared.exception;

import com.michaels.aggregationgateway.shared.common.BusinessCode;
import org.springframework.http.HttpStatus;

public class DuplicateException extends MikException {
    public DuplicateException(BusinessCode bc) {
        super(HttpStatus.CONFLICT, bc);
    }
}
