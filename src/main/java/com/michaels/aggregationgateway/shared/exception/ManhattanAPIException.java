package com.michaels.aggregationgateway.shared.exception;

import com.michaels.aggregationgateway.shared.common.BusinessCode;

public class ManhattanAPIException extends Exception {

    private BusinessCode bc;
    private String customMessage;

    public ManhattanAPIException(BusinessCode bc, String message) {
        this.bc = bc;
        this.customMessage = message;
    }

    public ManhattanAPIException(BusinessCode bc) {
        this.bc = bc;
    }

    @Override
    public String getMessage() {
        return this.customMessage==null? bc.getMessage(): this.customMessage;
    }

    public void setMessage(String customMessage) {
        this.customMessage = customMessage;
    }

    public ManhattanAPIException(String message) {
        this.bc = BusinessCode.MCU_INTERNAL_ERROR;
    }

    public String getCode(){
        return this.bc.getCode();
    }

}
