package com.michaels.aggregationgateway.shared.exception;

import com.michaels.aggregationgateway.shared.common.BusinessCode;
import org.springframework.http.HttpStatus;

public class InvalidCredentialsException extends MikException {

  public InvalidCredentialsException(BusinessCode bc, String message) {
    super(HttpStatus.UNAUTHORIZED, bc, message);
  }

  public InvalidCredentialsException(BusinessCode bc) {
    super(HttpStatus.UNAUTHORIZED, bc);
  }
}
