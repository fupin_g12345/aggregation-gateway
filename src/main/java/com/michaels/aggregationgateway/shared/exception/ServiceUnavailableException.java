package com.michaels.aggregationgateway.shared.exception;

import com.michaels.aggregationgateway.shared.common.BusinessCode;
import org.springframework.http.HttpStatus;

public class ServiceUnavailableException extends MikException {

  public ServiceUnavailableException(BusinessCode bc) {
    super(HttpStatus.SERVICE_UNAVAILABLE, bc);
  }
}
