package com.michaels.aggregationgateway.shared.exception;

import com.michaels.aggregationgateway.shared.common.BusinessCode;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.http.HttpStatus;

public class MikException extends ResponseStatusException {

  private BusinessCode bc;
  private String customMessage;

  public MikException(HttpStatus status, BusinessCode bc, String message) {
    super(status);
    this.bc = bc;
    this.customMessage = message;
  }

  public MikException(HttpStatus status, BusinessCode bc) {
    super(status);
    this.bc = bc;
  }

  @Override
  public String getMessage() {
    return this.customMessage == null ? bc.getMessage() : this.customMessage;
  }

  public void setMessage(String customMessage) {
    this.customMessage = customMessage;
  }

  public MikException(String message) {
    super(HttpStatus.INTERNAL_SERVER_ERROR, message);
    this.bc = BusinessCode.MCU_INTERNAL_ERROR;
  }

  public MikException(String message, Throwable cause) {
    super(HttpStatus.INTERNAL_SERVER_ERROR, message, cause);
    this.bc = BusinessCode.MCU_INTERNAL_ERROR;
  }

  public String getCode() {
    return this.bc.getCode();
  }
}
