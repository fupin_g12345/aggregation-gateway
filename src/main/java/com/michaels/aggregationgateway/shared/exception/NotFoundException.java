package com.michaels.aggregationgateway.shared.exception;

import com.michaels.aggregationgateway.shared.common.BusinessCode;
import org.springframework.http.HttpStatus;

public class NotFoundException extends MikException {
  @Deprecated
  public NotFoundException(BusinessCode bc, String message) {
    super(HttpStatus.NOT_FOUND, bc, message);
  }
  public NotFoundException(BusinessCode bc) {
    super(HttpStatus.NOT_FOUND, bc);
  }
}
