package com.michaels.aggregationgateway.shared.exception;

import com.michaels.aggregationgateway.shared.common.BusinessCode;
import org.springframework.http.HttpStatus;

public class UserLockedException extends MikException {

  private Long unlockTimeInMillis;

  public UserLockedException(BusinessCode bc, String message) {
    super(HttpStatus.FORBIDDEN, bc, message);
  }

  public UserLockedException(BusinessCode bc) {
    super(HttpStatus.FORBIDDEN, bc);
  }

  public Long getUnlockTimeInMillis() {
    return unlockTimeInMillis;
  }

  public void setUnlockTimeInMillis(Long unlockTimeInMillis) {
    this.unlockTimeInMillis = unlockTimeInMillis;
  }
}

