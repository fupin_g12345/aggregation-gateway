package com.michaels.aggregationgateway.admin.security.auth;

public class AdminAuthContext {

  public static final String _userId = "userId";
  public static final String _token = "token";
  public static final String _createTime = "createTime";
  public static final String _expireTime = "expireTime";

  public static final ThreadLocal<AdminAuthContext> threadLocalInstanceHolder = new ThreadLocal<AdminAuthContext>();

  public static void setAuthContext(AdminAuthContext adminAuthContext) {
    threadLocalInstanceHolder.set(adminAuthContext);
  }

  public static AdminAuthContext get() {
    return threadLocalInstanceHolder.get();
  }

  private Long userId;
  private String token;
  private Long createTime;
  private Long expireTime;

  public boolean hasExpired() {
    return System.currentTimeMillis() - expireTime > 0;
  }

  public Long getUserId() {
    return userId;
  }

  public void setUserId(Long userId) {
    this.userId = userId;
  }

  public String getToken() {
    return token;
  }

  public void setToken(String token) {
    this.token = token;
  }

  public Long getCreateTime() {
    return createTime;
  }

  public void setCreateTime(Long createTime) {
    this.createTime = createTime;
  }

  public Long getExpireTime() {
    return expireTime;
  }

  public void setExpireTime(Long expireTime) {
    this.expireTime = expireTime;
  }
}
