  package com.michaels.aggregationgateway.admin.security.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

  @Component
  public class AdminJwtConfig {

    @Value("${admin.jwt.signingKey: X0fzBdI07jj/DVwI+tW6LhmTISlupnTI+s4EcJLoR6M=}")
    private String jwtSigningKey;

    public String getJwtSigningKey() {
      return jwtSigningKey;
    }

    public void setJwtSigningKey(String jwtSigningKey) {
      this.jwtSigningKey = jwtSigningKey;
    }

  }
