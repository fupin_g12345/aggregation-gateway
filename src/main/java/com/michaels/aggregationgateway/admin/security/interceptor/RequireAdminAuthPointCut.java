package com.michaels.aggregationgateway.admin.security.interceptor;

import com.michaels.aggregationgateway.admin.security.auth.AdminAuthContext;
import com.michaels.aggregationgateway.shared.common.BusinessCode;
import com.michaels.aggregationgateway.shared.exception.InvalidCredentialsException;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class RequireAdminAuthPointCut {

  @Pointcut("@annotation(com.michaels.aggregationgateway.admin.security.annotation.RequireAdminAuth)")
  public void requireAdminAuthMethods() {

  }

  @Before("requireAdminAuthMethods()")
  public void requireAdminAuthMethodsBefore(JoinPoint joinPoint) {
    checkRequireAdminAuth(joinPoint);
  }

  @Pointcut("@within(com.michaels.aggregationgateway.admin.security.annotation.RequireAdminAuth)")
  public void requireAdminAuthClasses() {

  }

  @Before("requireAdminAuthClasses()")
  public void requireAdminAuthClassesBefore(JoinPoint joinPoint) {
    checkRequireAdminAuth(joinPoint);
  }

  private void checkRequireAdminAuth(JoinPoint joinPoint) {
    AdminAuthContext adminAuthContext = AdminAuthContext.get();
    if (adminAuthContext == null) {
      throw new InvalidCredentialsException(BusinessCode.MCU_UNAUTHORIZED_ACCESS);
    }
  }

}
