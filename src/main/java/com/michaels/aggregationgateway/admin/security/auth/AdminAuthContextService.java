package com.michaels.aggregationgateway.admin.security.auth;


import com.michaels.aggregationgateway.admin.security.configuration.AdminJwtConfig;
import com.michaels.aggregationgateway.shared.util.StringUtils;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;

@Service
public class AdminAuthContextService {

  @Autowired
  private AdminJwtConfig adminJwtConfig;

  public String toJWT(AdminAuthContext adminAuthContext) {
    Map<String, Object> claims=new LinkedHashMap<String, Object>();
    claims.put(AdminAuthContext._userId, StringUtils.nonull(adminAuthContext.getUserId()));
    claims.put(AdminAuthContext._token, adminAuthContext.getToken());
    claims.put(AdminAuthContext._createTime, String.valueOf(adminAuthContext.getCreateTime()));
    claims.put(AdminAuthContext._expireTime, String.valueOf(adminAuthContext.getExpireTime()));

    String jwtToken=Jwts.builder()
        .setClaims(claims)
        .setSubject(Long.toString(adminAuthContext.getUserId()))
        .setIssuedAt(new Date(adminAuthContext.getCreateTime()))
        .setExpiration(new Date(adminAuthContext.getExpireTime()))
        .signWith(SignatureAlgorithm.HS256, adminJwtConfig.getJwtSigningKey())
        .compact();
    return jwtToken;
  }

  public AdminAuthContext fromJWT(String jwtToken) {
    Claims claims = Jwts.parser()
        .setSigningKey(adminJwtConfig.getJwtSigningKey())
        .parseClaimsJws(jwtToken)
        .getBody();

    Long userId = Long.parseLong(claims.get(AdminAuthContext._userId)+"");
    String token = String.valueOf(claims.get(AdminAuthContext._token));
    Long createTime = Long.valueOf(claims.get(AdminAuthContext._createTime)+"");
    Long expireTime = Long.valueOf(claims.get(AdminAuthContext._expireTime)+"");

    AdminAuthContext adminAuthContext =new AdminAuthContext();
    adminAuthContext.setUserId(userId);
    adminAuthContext.setToken(token);
    adminAuthContext.setCreateTime(createTime);
    adminAuthContext.setExpireTime(expireTime);

    return adminAuthContext;
  }

  public AdminJwtConfig getJwtConfig() {
    return adminJwtConfig;
  }

  public void setJwtConfig(AdminJwtConfig adminJwtConfig) {
    this.adminJwtConfig = adminJwtConfig;
  }
}
