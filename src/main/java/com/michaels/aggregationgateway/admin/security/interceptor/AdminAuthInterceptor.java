package com.michaels.aggregationgateway.admin.security.interceptor;

import com.michaels.aggregationgateway.admin.security.auth.AdminAuthContext;
import com.michaels.aggregationgateway.admin.security.auth.AdminAuthContextService;
import com.michaels.aggregationgateway.shared.util.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebFilter("/*")
@Component
public class AdminAuthInterceptor extends HttpFilter {

  private static final String BEARER_PREFIX="Bearer ";

  @Override
  protected void doFilter(HttpServletRequest request, HttpServletResponse response, FilterChain chain)
      throws IOException, ServletException {

    // attach the AuthContext to current thread, if any
    try {
      String token = request.getHeader("Admin-Authorization");

      if(!StringUtils.isEmpty(token)) {
        if(StringUtils.startsWithIgnoreCase(token, BEARER_PREFIX)) {
          token=token.substring(BEARER_PREFIX.length());
        }

        WebApplicationContext context = WebApplicationContextUtils.getRequiredWebApplicationContext(request.getServletContext());
        AdminAuthContextService adminAuthContextService = context.getBean(AdminAuthContextService.class);
        AdminAuthContext adminAuthContext = adminAuthContextService.fromJWT(token);
        adminAuthContext = adminAuthContext.hasExpired()?null: adminAuthContext;
        AdminAuthContext.setAuthContext(adminAuthContext);
      } else {
        AdminAuthContext.setAuthContext(null);
      }

    } catch (Exception e) {
      AdminAuthContext.setAuthContext(null);
    }

    try {
      // process
      super.doFilter(request, response, chain);

    } finally {
      // clean
      AdminAuthContext.setAuthContext(null);
    }
  }

  
}
