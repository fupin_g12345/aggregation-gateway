package com.michaels.aggregationgateway.admin.security.interceptor;

import com.michaels.aggregationgateway.admin.security.auth.AdminAuthContext;
import com.michaels.aggregationgateway.security.auth.AuthContext;
import com.michaels.aggregationgateway.shared.common.BusinessCode;
import com.michaels.aggregationgateway.shared.exception.BadRequestException;
import com.michaels.aggregationgateway.shared.exception.InvalidCredentialsException;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class RequireComposeAuthPointCut {
  private static final Logger LOGGER = LoggerFactory.getLogger(RequireComposeAuthPointCut.class);

  @Pointcut("@annotation(com.michaels.aggregationgateway.admin.security.annotation.RequireComposeAuth)")
  public void requireComposeAuthMethods() {

  }

  @Before("requireComposeAuthMethods()")
  public void requireComposeAuthMethodsBefore(JoinPoint joinPoint) {
    checkRequireComposeAuth(joinPoint);
  }

  @Pointcut("@within(com.michaels.aggregationgateway.admin.security.annotation.RequireComposeAuth)")
  public void requireComposeAuthClasses() {

  }

  @Before("requireComposeAuthClasses()")
  public void requireComposeAuthClassesBefore(JoinPoint joinPoint) {
    checkRequireComposeAuth(joinPoint);
  }

  private void checkRequireComposeAuth(JoinPoint joinPoint) {
    AuthContext authContext = AuthContext.get();
    AdminAuthContext adminAuthContext = AdminAuthContext.get();
    Long sellerStoreId;
    if (joinPoint.getArgs()[0] instanceof Long) {
      sellerStoreId = (Long)joinPoint.getArgs()[0];
    } else if (joinPoint.getArgs()[0] instanceof String) {
      sellerStoreId = Long.valueOf((String)joinPoint.getArgs()[0]);
    } else {
      LOGGER.error("authorized param error, param:{}", joinPoint.getArgs()[0].toString());
      throw new InvalidCredentialsException(BusinessCode.MCU_ADMIN_AUTHORIZED_PARAM_ERROR);
    }

    if (sellerStoreId.equals(0L)) {
      if (adminAuthContext == null) {
        throw new InvalidCredentialsException(BusinessCode.MCU_UNAUTHORIZED_ACCESS);
      }
    } else {
      if(authContext == null && adminAuthContext == null) {
        throw new InvalidCredentialsException(BusinessCode.MCU_UNAUTHORIZED_ACCESS);
      }

      if (adminAuthContext != null) {
        return;
      }

      if (authContext != null) {
        if (!sellerStoreId.equals(authContext.getSellerStoreId())) {
          throw new BadRequestException(BusinessCode.MCU_UNAUTHORIZED_ACCESS);
        }
      }
    }
  }
}
